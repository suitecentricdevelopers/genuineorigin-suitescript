function beforeSubmit(type) {
        nlapiLogExecution('DEBUG', 'in set method');

  var context = nlapiGetContext().getExecutionContext();
          nlapiLogExecution('DEBUG', 'type', JSON.stringify(type));
          nlapiLogExecution('DEBUG', 'context', context);

  if (type == 'create' && context == 'webstore') {
    try {
      var record = nlapiGetNewRecord();
      var ups =  nlapiGetContext().getSetting('SCRIPT', 'custscript_get_ups_ground');
      var ltl =  nlapiGetContext().getSetting('SCRIPT', 'custscript_get_ltl');
      var ltlgate = nlapiGetContext().getSetting('SCRIPT', 'custscript_get_ltl_gate');
      var freightIndoor = nlapiGetContext().getSetting('SCRIPT', 'custscript_get_freight_indoor');
      var shippingAddressId = record.getFieldValue('shipaddresslist');
      var shipmethod = record.getFieldValue('shipmethod');
      var shipto = record.getFieldValue('shipaddress');
      record.setFieldValue('custbody_cust_shipping_address', shipto);
          
      nlapiLogExecution('DEBUG', 'shipto', JSON.stringify(shipto));
      nlapiLogExecution('DEBUG', 'ups', JSON.stringify(ups));
      nlapiLogExecution('DEBUG', 'ltlgate', JSON.stringify(ltlgate));
      nlapiLogExecution('DEBUG', 'freightIndoor', JSON.stringify(freightIndoor));
      nlapiLogExecution('DEBUG', 'ltl', JSON.stringify(ltl));    
      nlapiLogExecution('DEBUG', 'shippingAddressId', JSON.stringify(shippingAddressId));
      nlapiLogExecution('DEBUG', 'shipmethod 1', JSON.stringify(shipmethod));
      //var eastWarehouse = ['AL','AR','CT','DE','FL','GA','IL','IN','IA','KS','KY','LA','ME','MD','MA','MI','MN','MS','MO',
      //			'NE','NH','NJ','NY','NC','OH','OK','PA','RI','SC','TN','TX','VT','VA','WV','WI'];
      var westWarehouseStates = ['AZ','CA','CO','ID','MT','ND','NM','NV','OR','SD','UT','WA','WY']; 
      
      var shipstate = record.getFieldValue('shipstate')
      nlapiLogExecution('DEBUG', 'shipstate', JSON.stringify(shipstate));
      
      //define preferred warehouse
      var preferredWarehouse;
      if (shipstate) {
        if (westWarehouseStates.indexOf(shipstate) > -1) {
          preferredWarehouse = 'West Coast Warehouse';      
        } else {
          preferredWarehouse = 'East Coast Warehouse';
        }
      }
      
      //set preferred warehouse to record
      record.setFieldValue('custbody_customer_preferred_warehouse', preferredWarehouse);  

          
      
      var itemCount = record.getLineItemCount('item');
      nlapiLogExecution('DEBUG', 'itemCount', JSON.stringify(itemCount));

      record.setFieldValue('ismultishipto', 'T');
      
      var itemsPreferArray = [];
      var backorderCost = 0;
      var backorderhandingfee = 0;
     
      for (var line = 1; line < itemCount + 1; line++) {
        var itemPreferDetails = {
				'itemId': '',
				'itemQty':0,
				'isBackOrder': false,
        'shipFromEast': false,
        'shipFromWest':false,
        'shipFromWestandEast':false,
        'isSample': false,
        'isShippingItem' : false,
        'shipFrom': '',
        'shipEastqty': 0,
        'shipWestqty' : 0,
        'addHangingFee': false,
        'shippingMethod': '',
        'defaultShippingCost': null
			  }
        itemPreferDetails.itemId = record.getLineItemValue('item', 'item', line);
        itemPreferDetails.itemQty = Number(record.getLineItemValue('item', 'quantity',line));
        itemPreferDetails.isSample = record.getLineItemValue('item', 'custcol3', line) === '2' ? true : false;
        itemPreferDetails.defaultShippingCost = getItemDefaultShippingCost(itemPreferDetails.itemId);
        nlapiLogExecution('DEBUG', 'back order 1', JSON.stringify(record.getLineItemValue('item','quantitybackordered',line)));
        nlapiLogExecution('DEBUG', 'back order 3', JSON.stringify(record.getLineItemValue('item','backordered',line)));
        nlapiLogExecution('DEBUG', 'itemPreferDetails.defaultShippingCost', JSON.stringify(itemPreferDetails.defaultShippingCost));

  
        itemPreferDetails.isBackOrder = record.getLineItemValue('item','backordered',line).toString() > 0 ? true : false;
    
        
        nlapiLogExecution('DEBUG', 'back order 2', JSON.stringify(itemPreferDetails.isBackOrder));



        record.selectLineItem('item', line);

        if (itemPreferDetails.isBackOrder) {
          // check if the item Qty is over 10
          if (Number(itemPreferDetails.itemQty) > 9) {      
            if (shipmethod === ups) {
              record.setCurrentLineItemValue('item', 'custcol_item_shipping_method','UPS Ground'); 
              record.setCurrentLineItemValue('item','shipmethod',ups);
              backorderCost = backorderCost + Number(itemPreferDetails.itemQty) * 10;
            } 
            if (shipmethod === ltl) {
              record.setCurrentLineItemValue('item', 'custcol_item_shipping_method','UPS LTL Freight');
              record.setCurrentLineItemValue('item','shipmethod',ltl);
              backorderCost = backorderCost + 0;
            }
            if (shipmethod === ltlgate) {
              record.setCurrentLineItemValue('item', 'custcol_item_shipping_method', 'UPS LTL with Liftgate');
              record.setCurrentLineItemValue('item','shipmethod',ltlgate);
              backorderCost = backorderCost + 0;
              backorderhandingfee = backorderhandingfee + 25;
            }
            if (shipmethod === freightIndoor) {
              record.setCurrentLineItemValue('item', 'custcol_item_shipping_method', 'Freight+Indoor Delivery');
              record.setCurrentLineItemValue('item','shipmethod',freightIndoor);
              backorderCost = backorderCost + Number(itemPreferDetails.itemQty) * 5;
            }
            
            record.setCurrentLineItemValue('item','location','26');

            itemPreferDetails.shippingMethod = shipmethod;
          } else {
            record.setCurrentLineItemValue('item', 'custcol_item_shipping_method','UPS Ground'); 
            record.setCurrentLineItemValue('item','shipmethod',ups);
            record.setCurrentLineItemValue('item','location','26');
            backorderCost = backorderCost + Number(itemPreferDetails.itemQty) * 10;

            itemPreferDetails.shippingMethod = ups;
          }
          
          nlapiLogExecution('DEBUG', 'back order');

          
          record.setCurrentLineItemValue('item','custcol_warehouse','Back Order');
          record.setCurrentLineItemValue('item','shipaddress',shippingAddressId);

        } else if (itemPreferDetails.isSample) {
          
          record.setCurrentLineItemValue('item','custcol_item_shipping_method','UPS Ground'); 
          record.setCurrentLineItemValue('item','shipmethod',ups);
          record.setCurrentLineItemValue('item','shipaddress',shippingAddressId);
          record.setCurrentLineItemValue('item','custcol_warehouse','Genuine Origin Warehouse');
          record.setCurrentLineItemValue('item','location','6');

          itemPreferDetails.shippingMethod = ups;
          
          nlapiLogExecution('DEBUG', 'sample');

          
        } else {
          //get ship from warehouse
          var warehouseQty = getItemWareHouseQty(itemPreferDetails.itemId);
          var shippingInfo = getItemShipInfo(itemPreferDetails.itemQty, Number(warehouseQty.westQty), Number(warehouseQty.eastQty), preferredWarehouse);
          
          
          nlapiLogExecution('DEBUG', 'shippingCost', JSON.stringify(shippingInfo));
          itemPreferDetails.shipFromEast = shippingInfo.shipFromEast;
          itemPreferDetails.shipFromWest = shippingInfo.shipFromWest;
          itemPreferDetails.shipFrom = shippingInfo.shipFrom;
          itemPreferDetails.shipFromWestandEast = shippingInfo.shipFromWestandEast;
          itemPreferDetails.shipEastqty = shippingInfo.shipEastqty;
          itemPreferDetails.shipWestqty = shippingInfo.shipWestqty;
          
          nlapiLogExecution('DEBUG', 'east west');

          
          itemsPreferArray.push(itemPreferDetails);

        }
        
        record.setCurrentLineItemValue('item','custcol_line_item_shipping_cost',JSON.stringify(itemPreferDetails)); 
        record.commitLineItem('item');
  
      }
      
      nlapiLogExecution('DEBUG', 'backorderCost', JSON.stringify(backorderCost));
      nlapiLogExecution('DEBUG', 'backorderhandingfee', JSON.stringify(backorderhandingfee));


      
      var eastWarehouseItems = [];
      var westWarehouseItems = [];
      var eastOrderTotalQty = 0;
      var westOrderTotalQty = 0;
      var eastCost = 0;
      var westCost = 0;
      var handlingrateeast = 0;
      var handlingratewest = 0;
      // var eastNoneDefaultShipCostQty = 0;
			// var westNoneDefaultShipCostQty = 0; 
      var hasDefaultShipping = [];
      
      nlapiLogExecution('DEBUG', 'itemsPreferArray', JSON.stringify(itemsPreferArray));
      
      if (itemsPreferArray && itemsPreferArray.length) {
        eastWarehouseItems = _.groupBy(itemsPreferArray, 'shipFromEast')[true]; 
        westWarehouseItems = _.groupBy(itemsPreferArray, 'shipFromWest')[true]; 
      }
      
      if (westWarehouseItems && westWarehouseItems.length) {
        westOrderTotalQty = westWarehouseItems.reduce(function (total, line) {
            return total + line.shipWestqty;
        }, 0); 
        
        var westNoneDefaultShipCostQty = westOrderTotalQty;
        
        for (var west1 = 0; west1 < westWarehouseItems.length; west1++) {
						if (!!westWarehouseItems[west1].defaultShippingCost ||westWarehouseItems[west1].defaultShippingCost === "0.00") {
							 hasDefaultShipping.push(westWarehouseItems[west1]);
							 westNoneDefaultShipCostQty = westNoneDefaultShipCostQty - westWarehouseItems[west1].itemQty;
						}
						
				}    
         
      }
      
      if (eastWarehouseItems && eastWarehouseItems.length) {
        eastOrderTotalQty = eastWarehouseItems.reduce(function (total, line) {
            return total + line.shipEastqty;
        }, 0);
        
        var eastNoneDefaultShipCostQty = eastOrderTotalQty;
        
        for (var east1 = 0; east1 < eastWarehouseItems.length; east1++) {
						if (!!eastWarehouseItems[east1].defaultShippingCost ||eastWarehouseItems[east1].defaultShippingCost === "0.00") {
							 hasDefaultShipping.push(eastWarehouseItems[east1]);
							 eastNoneDefaultShipCostQty = eastNoneDefaultShipCostQty - eastWarehouseItems[east1].itemQty;
						}
						
					}
      }
      
      if (eastOrderTotalQty && eastOrderTotalQty!==0) {
        if (preferredWarehouse === 'East Coast Warehouse') {
          
          if (eastOrderTotalQty > 9) {
            if (shipmethod === ups) {
              eastCost = eastNoneDefaultShipCostQty*10;
            }
            if (shipmethod === ltl) {
              eastCost = 0;
            }
            
            if (shipmethod === ltlgate) {
              eastCost = 0;
              handlingrateeast = 25;
            }
          
            if (shipmethod === freightIndoor) {
              eastCost = eastNoneDefaultShipCostQty*5;
            }
          } else {
              eastCost = eastNoneDefaultShipCostQty*10;
          }
          
        
        } else {
          if (eastOrderTotalQty > 9) {
            if (shipmethod === ups) {
              eastCost = eastNoneDefaultShipCostQty*20;
            }
            if (shipmethod === ltl) {
              eastCost = eastNoneDefaultShipCostQty *10;
            }
            if (shipmethod === ltlgate) {
              eastCost = eastNoneDefaultShipCostQty *10;
              handlingrateeast = 25;
            }      
            if (shipmethod === freightIndoor) {
              eastCost = eastNoneDefaultShipCostQty*15;
            }
          } else {
            eastCost = eastNoneDefaultShipCostQty*20;
          }
          
        }
      }
      
      nlapiLogExecution('DEBUG', 'eastCost 2', JSON.stringify(eastCost));  
      
      nlapiLogExecution('DEBUG', 'handlingrateeast 2', JSON.stringify(handlingrateeast));  
  
  

      
      if (westOrderTotalQty && westOrderTotalQty !==0) {
          if (preferredWarehouse === 'West Coast Warehouse') {
            if (westOrderTotalQty > 9) {
              if (shipmethod === ups) {
                westCost = westNoneDefaultShipCostQty * 10;
              }
              if (shipmethod === ltl) {
                westCost = 0;
              }
              
              if (shipmethod === ltlgate) {
                westCost = 0;
                handlingratewest = 25;
              }
          
              if (shipmethod === freightIndoor) {
                westCost = westNoneDefaultShipCostQty * 5;
              }
            } else {
              westCost = westNoneDefaultShipCostQty * 10;
            }
                
          } else {
            if (westOrderTotalQty > 9) {
              if (shipmethod === ups) {
                westCost = westNoneDefaultShipCostQty * 20;
              }
              if (shipmethod === ltl) {
                westCost = westNoneDefaultShipCostQty * 10;
              }
              
              if (shipmethod === ltlgate) {
                westCost = westNoneDefaultShipCostQty * 10;
                handlingratewest = 25;
              }
          
              if (shipmethod === freightIndoor) {
                westCost = westNoneDefaultShipCostQty * 15;
              }
            } else {
              westCost = westNoneDefaultShipCostQty * 20;
            }
            
          }
      }
      
      nlapiLogExecution('DEBUG', 'westNoneDefaultShipCostQty 2', JSON.stringify(westNoneDefaultShipCostQty)); 
      nlapiLogExecution('DEBUG', 'handlingratewest 2', JSON.stringify(handlingratewest));  
      nlapiLogExecution('DEBUG', 'eastNoneDefaultShipCostQty 2', JSON.stringify(eastNoneDefaultShipCostQty));  

      nlapiLogExecution('DEBUG', 'eastCost 2', JSON.stringify(eastCost));  
      nlapiLogExecution('DEBUG', 'eastCost 2', JSON.stringify(westCost));  


      
      var totaldefaultshippingcost = 0;
      
      if (hasDefaultShipping && hasDefaultShipping.length) {
        totaldefaultshippingcost = hasDefaultShipping.reduce(function (total, line) {
            return total + Number(line.defaultShippingCost)*line.itemQty;
        }, 0);
      }  
 
      nlapiLogExecution('DEBUG', '------totaldefaultshippingcost-----', JSON.stringify(totaldefaultshippingcost));  

      var shippingTotalcost =  westCost + eastCost +  backorderCost + totaldefaultshippingcost;
      var handlingTotalrate = handlingratewest + handlingrateeast + backorderhandingfee;

      nlapiLogExecution('DEBUG', '------shippingTotalcost-----', JSON.stringify(shippingTotalcost));  
      nlapiLogExecution('DEBUG', '------handlingTotalrate-----', JSON.stringify(handlingTotalrate));
      
      record.setFieldValue('shippingcost', shippingTotalcost);
      record.setFieldValue('handlingcost', handlingTotalrate);
      
      nlapiLogExecution('DEBUG', '------sgetFieldValue hippingTotalcost-----', record.getFieldValue('shippingcost')); 
      nlapiLogExecution('DEBUG', '------sgetFieldValue handlingTotalrate-----', record.getFieldValue('handlingcost'));  

      // check east warehouse items
      if (eastWarehouseItems && eastWarehouseItems.length) {
    
        nlapiLogExecution('DEBUG', 'eastWarehouseItems 2', JSON.stringify(eastOrderTotalQty));    
				for (var east = 0; east < eastWarehouseItems.length; east++) {
          for (var eastItem = 1; eastItem < itemCount + 1; eastItem++) {
            if (eastWarehouseItems[east].itemId === record.getLineItemValue('item', 'item', eastItem)) {
              record.selectLineItem('item', eastItem);

              if (eastOrderTotalQty > 9) {
                eastWarehouseItems[east].shippingMethod = shipmethod;
                record.setCurrentLineItemValue('item','shipmethod',shipmethod); 
                if (shipmethod === ups) {
                  record.setCurrentLineItemValue('item','custcol_item_shipping_method','UPS Ground'); 
                }
                if (shipmethod === ltlgate) {
                  record.setCurrentLineItemValue('item','custcol_item_shipping_method','UPS LTL Freight'); 
                }
                if (shipmethod === ltl) {
                  record.setCurrentLineItemValue('item','custcol_item_shipping_method','UPS LTL with Liftgate'); 
                }
                if (shipmethod === freightIndoor) {
                  record.setCurrentLineItemValue('item','custcol_item_shipping_method','Freight+Indoor Delivery'); 
                }
     
              } else {
                eastWarehouseItems[east].shippingMethod = ups;
                record.setCurrentLineItemValue('item','shipmethod',ups);
                record.setCurrentLineItemValue('item','custcol_item_shipping_method','UPS Ground'); 

              }
              record.setCurrentLineItemValue('item','location','17');
              record.setCurrentLineItemValue('item','shipaddress',shippingAddressId);
              record.setCurrentLineItemValue('item','custcol_warehouse', eastWarehouseItems[east].shipFrom);
              record.setCurrentLineItemValue('item','custcol_line_item_shipping_cost',JSON.stringify(eastWarehouseItems[east])); 
              record.commitLineItem('item');
            }
        
          }  
				}  
			}
			
			if (westWarehouseItems && westWarehouseItems.length) {
        
        westOrderTotalQty = westWarehouseItems.reduce(function (total, line) {
            return total + line.shipWestqty;
        }, 0);
        
        nlapiLogExecution('DEBUG', 'westOrderTotalQty 2', JSON.stringify(westOrderTotalQty));

        
				for (var west = 0; west < westWarehouseItems.length; west++) {
          for (var westItem = 1; westItem < itemCount + 1; westItem++) {
            if (westWarehouseItems[west].itemId === record.getLineItemValue('item', 'item', westItem)) {
              record.selectLineItem('item', westItem);

              if (westOrderTotalQty > 9) {
                westWarehouseItems[west].shippingMethod = shipmethod;
                record.setCurrentLineItemValue('item','shipmethod',shipmethod);
                if (shipmethod === ups) {
                  record.setCurrentLineItemValue('item','custcol_item_shipping_method','UPS Ground'); 
                }
                if (shipmethod === ltlgate) {
                  record.setCurrentLineItemValue('item','custcol_item_shipping_method','UPS LTL Freight'); 
                }
                if (shipmethod === ltl) {
                  record.setCurrentLineItemValue('item','custcol_item_shipping_method','UPS LTL with Liftgate'); 
                }
                if (shipmethod === freightIndoor) {
                  record.setCurrentLineItemValue('item','custcol_item_shipping_method','Freight+Indoor Delivery'); 
                } 
      
              } else {
                westWarehouseItems[west].shippingMethod = ups;
                record.setCurrentLineItemValue('item','shipmethod',ups);
                record.setCurrentLineItemValue('item','custcol_item_shipping_method','UPS Ground');

              }
              record.setCurrentLineItemValue('item','location','18');
              record.setCurrentLineItemValue('item','shipaddress',shippingAddressId);
              record.setCurrentLineItemValue('item','custcol_warehouse',westWarehouseItems[west].shipFrom);
              record.setCurrentLineItemValue('item','custcol_line_item_shipping_cost',JSON.stringify(westWarehouseItems[west])); 
              
              record.commitLineItem('item');
            }
        
          }
				}
    
			}
      
      var totalShipipngHandling = shippingTotalcost + handlingTotalrate;
      var shippingHandingItem = {
        'itemId': '',
        'itemQty':0,
        'isBackOrder': false,
        'shipFromEast': false,
        'shipFromWest':false,
        'shipFromWestandEast':false,
        'isSample': false,
        'isShippingItem' : true,
        'shipFrom': '',
        'shipEastqty': 0,
        'shipWestqty' : 0,
        'addHangingFee': false,
        'shippingMethod': ''
      }
      
      record.selectNewLineItem('item');
      record.setCurrentLineItemValue('item','item','9291');
      
      record.setCurrentLineItemValue('item','taxcode','-7');
      record.setCurrentLineItemValue('item','quantity',1);
      record.setCurrentLineItemValue('item','rate',totalShipipngHandling);
      record.setCurrentLineItemValue('item','custcol_line_item_shipping_cost', JSON.stringify(shippingHandingItem));
      
      record.commitLineItem('item');
        
      
    } catch(e) {
      nlapiLogExecution('ERROR', 'genuineorigin.ue.setItemLineFields before submit', e);
    }
  }
}
 

function getItemWareHouseQty (id) {
  var warehouseQty = {},
  
  eastwarehouseQty = nlapiLookupField('lotnumberedinventoryitem', id, 'custitem_east_warhouse_qty'),
  westwarehouseQty = nlapiLookupField('lotnumberedinventoryitem', id, 'custitem_west_warehouse_qty');
  
  warehouseQty.eastQty = !!eastwarehouseQty ? Number(eastwarehouseQty) : 0;

  warehouseQty.westQty = !!westwarehouseQty ? Number(westwarehouseQty) : 0;

  return warehouseQty;
}

function getItemDefaultShippingCost(id) {
  var record = nlapiLoadRecord('lotnumberedinventoryitem', id);
  var defaultShippingCost = record.getFieldValue('shippingcost');
  return defaultShippingCost;
}


function getItemShipInfo (itemOrderQty, westWarehouseQty, eastWarehouseQty, preferedWarehouse) {
  var shippingInfo = {
    'shipFrom': '',
    'shipFromEast' : false,
    'shipFromWest' : false,
    'shipFromWestandEast' : false,
    'shipEastqty': 0,
    'shipWestqty' : 0
  };
  var overPreferedQty;
  nlapiLogExecution('DEBUG', 'shippingCosts itemOrderQty', itemOrderQty);
  nlapiLogExecution('DEBUG', 'shippingCosts westWarehouseQty', westWarehouseQty);
  nlapiLogExecution('DEBUG', 'shippingCosts eastWarehouseQty', eastWarehouseQty);
  nlapiLogExecution('DEBUG', 'shippingCosts preferedWarehouse', JSON.stringify(preferedWarehouse));
  if (westWarehouseQty > 0 && eastWarehouseQty > 0) {
    if (preferedWarehouse === 'West Coast Warehouse') {
      
      shippingInfo.shipFromWest = true;

      //over west warehouse
      if (itemOrderQty > westWarehouseQty) {
        overPreferedQty = itemOrderQty - westWarehouseQty;  
        shippingInfo.shipFromEast = true;
        shippingInfo.shipWestqty = westWarehouseQty;
        shippingInfo.shipEastqty = overPreferedQty;
        shippingInfo.shipFrom = 'East Coast Warehouse, West Coast Warehouse';
      } else {
        shippingInfo.shipWestqty = itemOrderQty;
        shippingInfo.shipFrom = 'West Coast Warehouse';
      }
    }
    
    if (preferedWarehouse === 'East Coast Warehouse') {
      shippingInfo.shipFromEast = true;

      //over east warehouse
      if (itemOrderQty > eastWarehouseQty) {        
        overPreferedQty = itemOrderQty - eastWarehouseQty;
        shippingInfo.shipFromWest = true; 
        shippingInfo.shipEastqty = eastWarehouseQty;
        shippingInfo.shipWestqty = overPreferedQty; 
        shippingInfo.shipFrom = 'East Coast Warehouse, West Coast Warehouse';
      } else {
        shippingInfo.shipEastqty = itemOrderQty;
        shippingInfo.shipFrom = 'East Coast Warehouse';
      }
    }
  } else if (westWarehouseQty > 0 && eastWarehouseQty === 0) {
    // only has west
    shippingInfo.shipFrom = 'West Coast Warehouse';
    shippingInfo.shipFromWest = true;
    shippingInfo.shipWestqty = itemOrderQty;
  } else if (eastWarehouseQty > 0 && westWarehouseQty === 0 ) {
    //only east
    shippingInfo.shipFrom = 'East Coast Warehouse';
    shippingInfo.shipFromEast = true;
    shippingInfo.shipEastqty = itemOrderQty;
  }
  
  
  nlapiLogExecution('DEBUG', 'shippingInfo', JSON.stringify(shippingInfo));

  	return shippingInfo;	
	}	
  
  function afterSubmit(type) {
    nlapiLogExecution('DEBUG', 'in after method');

    var context = nlapiGetContext().getExecutionContext();
    
    if (type == 'create' && context == 'webstore') {
      try {
        var record = nlapiLoadRecord('salesorder',nlapiGetRecordId());
        var itemCount = record.getLineItemCount('item');      
        var getPreferredWarehouse = record.getFieldValue('custbody_customer_preferred_warehouse');    
        var shipgroupCount = record.getLineItemCount('shipgroup');
        var shippingAddressId = record.getLineItemValue('item', 'shipaddress', 1);
        
        nlapiLogExecution('DEBUG', 'itemCount', JSON.stringify(itemCount));
        nlapiLogExecution('DEBUG', 'shipgroupCount', JSON.stringify(shipgroupCount));
        nlapiLogExecution('DEBUG', 'getPreferredWarehouse', JSON.stringify(getPreferredWarehouse));

        var isSampleItems = [];
        var isBackOrder = [];
        var isEastShip = [];
        var isWestShip = [];
        var hasDefaultShipping = [];
        var ups =  nlapiGetContext().getSetting('SCRIPT', 'custscript_get_ups_ground');
        var ltl =  nlapiGetContext().getSetting('SCRIPT', 'custscript_get_ltl');
        var ltlgate = nlapiGetContext().getSetting('SCRIPT', 'custscript_get_ltl_gate');
        var freightIndoor = nlapiGetContext().getSetting('SCRIPT', 'custscript_get_freight_indoor');
        var addHangingFee = false;
        var shippingItem;
        var subtotal;
        
        
        // internalid 17 is East Warehouse; 18 is west warehouse; 6 is sample order
        for (var line = 1; line < itemCount + 1; line++) {
          var itemog = record.getLineItemValue('item', 'custcol_line_item_shipping_cost',line);
          
          nlapiLogExecution('DEBUG', 'itemog 0.0', itemog);
          
          var itemShipId = record.getLineItemValue('item', 'shipmethod', line);
          
          nlapiLogExecution('DEBUG', 'itemShipId', JSON.stringify(itemShipId));
          
          if (!!itemog) {
            var itemInfo = JSON.parse(itemog);
            

            nlapiLogExecution('DEBUG', 'itemInfo', JSON.stringify(itemInfo.itemId));
            
            //create back order shipping line 

              if (itemInfo.isBackOrder) {      
                isBackOrder.push(itemInfo);
                nlapiLogExecution('DEBUG', 'isBackOrder 2', JSON.stringify(isBackOrder));        
              } 
                nlapiLogExecution('DEBUG', 'isSampleItems 0');

              if (itemInfo.isSample) {
                  isSampleItems.push(itemInfo);
                  nlapiLogExecution('DEBUG', 'isSampleItems 2', JSON.stringify(isSampleItems));

              }
              if (itemInfo.shipFromEast) {
                  isEastShip.push(itemInfo);
                  nlapiLogExecution('DEBUG', 'isEastShip 2', JSON.stringify(isEastShip));

              }
              if (itemInfo.shipFromWest) {
                  isWestShip.push(itemInfo);
                  nlapiLogExecution('DEBUG', 'isWestShip 2', JSON.stringify(isWestShip));

               }
              if (itemInfo.isShippingItem) {
                 shippingItem = itemInfo;
                 subtotal = record.getFieldValue('newsubtotal');
                 var shipItemRate = record.getLineItemValue('item','amount',line);
                 var newsubtotal = Number(subtotal) - Number(shipItemRate);
                 record.removeLineItem('item',line);
                 record.setFieldValue('subtotal', newsubtotal);
              }
              
              if (!!itemInfo.defaultShippingCost || itemInfo.defaultShippingCost === 0) {
                hasDefaultShipping.push(itemInfo);
              }
            } 

        }
        
        nlapiLogExecution('DEBUG', 'isSampleItems 0', JSON.stringify(isSampleItems));
        nlapiLogExecution('DEBUG', 'isEastShip 0', JSON.stringify(isEastShip));
        nlapiLogExecution('DEBUG', 'isWestShip 0',JSON.stringify(isWestShip));
        nlapiLogExecution('DEBUG', 'isBackOrder 0', JSON.stringify(isBackOrder));
        nlapiLogExecution('DEBUG', 'shippingItem', JSON.stringify(shippingItem));

        
        var eastShippingCost = 0;
        var westShippingCost = 0;
        var ltlgateLineNum;
        // var hasDefaultShippingCostTotal = 0;
        var hasDefaultEastShipping = [];
        var hasDefaultWestShipping = [];
        
        var eastQty = isEastShip.reduce(function (total, line) {
            return total + line.shipEastqty;
        }, 0);
        
        var eastQtyNoDefaultShippingCostQty = eastQty;
        
        var westQty= isWestShip.reduce(function (total, line) {
              return total + line.shipWestqty;
        }, 0);
        
        var westQtyNoDefaultShippingCostQty = westQty;
        
        for (var e1 = 0; e1 < isEastShip.length; e1++) {
          if (!!isEastShip[e1].defaultShippingCost || isEastShip[e1].defaultShippingCost === 0) {
            eastQtyNoDefaultShippingCostQty = eastQtyNoDefaultShippingCostQty - isEastShip[e1].itemQty; 
            hasDefaultEastShipping.push(isEastShip[e1]);
          }
        }
        
        for (var w1 = 0; w1 < isWestShip.length; w1++) {
          if (!!isWestShip[w1].defaultShippingCost || isWestShip[w1].defaultShippingCost === 0) {
            westQtyNoDefaultShippingCostQty = westQtyNoDefaultShippingCostQty - isWestShip[w1].itemQty; 
            hasDefaultWestShipping.push(isWestShip[w1]);
          }
        }
        
        
       nlapiLogExecution('DEBUG', 'eastQtyNoDefaultShippingCostQty', JSON.stringify(eastQtyNoDefaultShippingCostQty));
       nlapiLogExecution('DEBUG', 'westQtyNoDefaultShippingCostQty', JSON.stringify(westQtyNoDefaultShippingCostQty));
        
        var eastDefaultShippingCostTotal = 0;
        var westDefaultShippingCostTotal = 0;
        if (hasDefaultEastShipping && hasDefaultEastShipping.length) {
          nlapiLogExecution('DEBUG', 'hasDefaultShipping');
        
          for (var de = 0; de < hasDefaultEastShipping.length; de++) {
            eastDefaultShippingCostTotal = eastDefaultShippingCostTotal + Number(hasDefaultEastShipping[de].defaultShippingCost)*hasDefaultEastShipping[de].itemQty;
          }
          nlapiLogExecution('DEBUG', 'hasDefaultShippingCostTotal',JSON.stringify(eastDefaultShippingCostTotal));
        }
        if (hasDefaultWestShipping && hasDefaultWestShipping.length) {
          nlapiLogExecution('DEBUG', 'hasDefaultShipping');
        
          for (var de1 = 0; de1 < hasDefaultWestShipping.length; de1++) {
            westDefaultShippingCostTotal = westDefaultShippingCostTotal + Number(hasDefaultWestShipping[de1].defaultShippingCost)*hasDefaultWestShipping[de1].itemQty;
          }
          nlapiLogExecution('DEBUG', 'hasDefaultShippingCostTotal',JSON.stringify(westDefaultShippingCostTotal));
        }
    
        if (eastQty && eastQty!==0) {
          if (getPreferredWarehouse === 'East Coast Warehouse') {
            if (isEastShip[0].shippingMethod === ups) {
              eastShippingCost = eastQtyNoDefaultShippingCostQty*10;
            }
            if (isEastShip[0].shippingMethod === ltl || isEastShip[0].shippingMethod === ltlgate) {
              eastShippingCost = 0;
            }
          
            if (isEastShip[0].shippingMethod === freightIndoor) {
              eastShippingCost = eastQtyNoDefaultShippingCostQty*5;
            }
          
          } else {
            if (isEastShip[0].shippingMethod === ups) {
              eastShippingCost = eastQtyNoDefaultShippingCostQty*20;
            }
            if (isEastShip[0].shippingMethod === ltl || isEastShip[0].shippingMethod === ltlgate) {
              eastShippingCost = eastQtyNoDefaultShippingCostQty *10;
            }
          
            if (isEastShip[0].shippingMethod === freightIndoor) {
              eastShippingCost = eastQtyNoDefaultShippingCostQty*15;
            }
          }
          
          eastShippingCost = eastShippingCost + eastDefaultShippingCostTotal;
        }
        
        nlapiLogExecution('DEBUG', 'eastShippingCost', JSON.stringify(eastShippingCost));

        
        if (westQty && westQty !==0) {
            if (getPreferredWarehouse === 'West Coast Warehouse') {
              if (isWestShip[0].shippingMethod === ups) {
                westShippingCost = westQtyNoDefaultShippingCostQty*10;
              }
              if (isWestShip[0].shippingMethod === ltl || isWestShip[0].shippingMethod === ltlgate) {
                westShippingCost = 0;
              }
          
              if (isWestShip[0].shippingMethod === freightIndoor) {
                westShippingCost = westQtyNoDefaultShippingCostQty*5;
              }
          
              nlapiLogExecution('DEBUG', ' getPreferredWarehouse west warehouse shipping line', JSON.stringify(westShippingCost));
          
            } else {
              if (isWestShip[0].shippingMethod === ups) {
                westShippingCost = westQtyNoDefaultShippingCostQty*20;
              }
              if (isWestShip[0].shippingMethod === ltl || isWestShip[0].shippingMethod === ltlgate) {
                westShippingCost = westQtyNoDefaultShippingCostQty *10;
              }
          
              if (isWestShip[0].shippingMethod === freightIndoor) {
                westShippingCost = westQtyNoDefaultShippingCostQty*15;
              }
            }
            
          westShippingCost = westShippingCost + westDefaultShippingCostTotal;
        }
        
        nlapiLogExecution('DEBUG', 'westShippingCost', JSON.stringify(westShippingCost));


        for (var shipline = 1; shipline < shipgroupCount + 1; shipline++) {
          nlapiLogExecution('DEBUG', 'loop');

          var shipFromLocation = record.getLineItemValue('shipgroup', 'sourceaddressref', shipline);
          var getShipMethod = record.getLineItemValue('shipgroup','shippingmethodref',shipline);
          nlapiLogExecution('DEBUG', 'getShipMethod 6', JSON.stringify(getShipMethod));


          record.selectLineItem('shipgroup',shipline);
          record.setCurrentLineItemValue('shipgroup','destinationaddressref',shippingAddressId);
          //set up sample warehouse cost
          if (shipFromLocation === '6') {
            nlapiLogExecution('DEBUG', 'shipFromLocation 6');
            
            record.setLineItemValue('shipgroup','shippingrate', shipline, '0');
 
            if (getShipMethod === ltlgate) {
              record.setLineItemValue('shipgroup','handlingrate', shipline, '25');
              nlapiLogExecution('DEBUG', 'addHangingFee 0', addHangingFee);
              nlapiLogExecution('DEBUG', 'addHangingFee 3', JSON.stringify(ltlgateLineNum));

            }
          }
          
          nlapiLogExecution('DEBUG', 'shipFromLocation 1');
          nlapiLogExecution('DEBUG', 'shipFromLocation0000000', JSON.stringify(shipFromLocation));

          
          //set up east warehouse cost
          if (shipFromLocation === '17') {
            nlapiLogExecution('DEBUG', 'shipFromLocation 17');
            nlapiLogExecution('DEBUG', 'shipFromLocation 17.2',JSON.stringify(eastShippingCost));
            

            // record.setCurrentLineItemValue('shipgroup','shippingrate',eastShippingCost.toString());
            record.setLineItemValue('shipgroup','shippingrate', shipline, eastShippingCost.toString());
            nlapiLogExecution('DEBUG', 'shipFromLocation 17.2',record.getLineItemValue('shipgroup','shippingrate',shipline));

            if (getShipMethod === ltlgate) {
              record.setLineItemValue('shipgroup','handlingrate', shipline, '25');
              nlapiLogExecution('DEBUG', 'addHangingFee 0', addHangingFee);
              nlapiLogExecution('DEBUG', 'addHangingFee 3', JSON.stringify(ltlgateLineNum));

            }
          }
          nlapiLogExecution('DEBUG', 'shipFromLocation 2');

          //set up west warehouse cost
          if (shipFromLocation === '18') {
            nlapiLogExecution('DEBUG', 'shipFromLocation 18');
            nlapiLogExecution('DEBUG', 'shipFromLocation 18.2',JSON.stringify(westShippingCost));

            record.setLineItemValue('shipgroup','shippingrate', shipline, westShippingCost.toString());

            if (getShipMethod === ltlgate) {
              record.setLineItemValue('shipgroup','handlingrate', shipline, '25');
              nlapiLogExecution('DEBUG', 'addHangingFee 0', addHangingFee);
              nlapiLogExecution('DEBUG', 'addHangingFee 3', JSON.stringify(ltlgateLineNum));

            }
          }
          nlapiLogExecution('DEBUG', 'shipFromLocation 3');

          //set up back office cost
          if (shipFromLocation === '26') {
            var backOrderHandlingfee = 0;
            //shipmethod is ups ground
            nlapiLogExecution('DEBUG', 'shipFromLocation 4');

            if (getShipMethod === ups) {    
              nlapiLogExecution('DEBUG', 'shipFromLocation 5+');
                 
              var backUpsArray = _.filter(isBackOrder,function(num){return num.shippingMethod === ups});
              var backUpsQty1 = 0;
              for (var b1 = 0; b1 < backUpsArray.length; b1++) {
                backUpsQty1 = backUpsArray[b1].itemQty+ backUpsQty1;
              }
                 nlapiLogExecution('DEBUG', 'backUpsQty1 18', backUpsQty1);

                 record.setLineItemValue('shipgroup','shippingrate', shipline, (backUpsQty1*10).toString());
            }
            
            if (getShipMethod === ltl || getShipMethod === ltlgate) {
              nlapiLogExecution('DEBUG', 'shipFromLocation ltl ltlgate');
              record.setLineItemValue('shipgroup','shippingrate', shipline, '0');
              
            }
            if (getShipMethod === freightIndoor) {
              nlapiLogExecution('DEBUG', 'shipFromLocation freightIndoor 0');
                 
                 var backfreightIndoorArray = _.filter(isBackOrder,function(num){return num.shippingMethod === freightIndoor});
                 var backfreightIndoorQty = 0;
                 for (var b2 = 0; b2 < backfreightIndoorArray.length; b2++) {
                   backfreightIndoorQty = backfreightIndoorArray[b2].itemQty+ backfreightIndoorQty;
                 }

                 nlapiLogExecution('DEBUG', 'backOrderCost freightIndoor', JSON.stringify(backfreightIndoorQty*5));

                 record.setLineItemValue('shipgroup','shippingrate', shipline, (backfreightIndoorQty*5).toString());
              nlapiLogExecution('DEBUG', 'shipFromLocation freightIndoor');

            }
            
            if (getShipMethod === ltlgate) {
              
              for (var back3 = 0; back3 < isBackOrder.length; back3++ ) {
                if (isBackOrder[back3].itemQty > 9) {
                  backOrderHandlingfee = backOrderHandlingfee + 25;
                }
              }
              
              nlapiLogExecution('DEBUG', 'backOrderHandlingfee 0', JSON.stringify(backOrderHandlingfee));

              record.setLineItemValue('shipgroup','handlingrate', shipline, backOrderHandlingfee);

            }
          }
          

          
        
        }
        
        nlapiSubmitRecord(record); 

        
      } catch (e){
        nlapiLogExecution('ERROR', 'genuineorigin.ue.setItemLineFields after submit', e);
      }
    }
    
  }
