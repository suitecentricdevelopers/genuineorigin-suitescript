/**
 * Set warehouse by quantity script
 * SuiteCentric LLC (hello@suitecentric.com)
 */

// How much governance should be remaining when the script is yielded
var GOVERNANCE_THRESHOLD = 100;

function main() {
  var productsRecords = getAllItemRecords();
  for (var i = 0; i < productsRecords.length; i++) {
    
    if (nlapiGetContext().getRemainingUsage() < GOVERNANCE_THRESHOLD) {
			nlapiLogExecution('DEBUG', 'Governance Reached', 'A new instance of this script will begin shortly.');
			nlapiYieldScript();
		}

		loadRecordLocation(productsRecords[i]);
  }
  
}

function getAllItemRecords() {
  var filter = [
    new nlobjSearchFilter('isinactive', null, 'is', 'F')
  ]
  
  var column = [
    new nlobjSearchColumn('internalid')
  ]
  
  var getSearchResult = suitecentric.Common.NetSuite.search('lotnumberedinventoryitem',null,filter,column);
  
  // nlapiLogExecution('DEBUG', 'getSearchResult', JSON.stringify(getSearchResult));
  return getSearchResult;
  
}

function loadRecordLocation(record) {
  nlapiLogExecution('DEBUG', 'record1');
  var id = record.getValue('internalid');
  var loadRecord = nlapiLoadRecord('lotnumberedinventoryitem', id);
  var locationLength;
  var productLocations = {};
  nlapiLogExecution('DEBUG', 'record2 length', loadRecord.getLineItemCount('locations'));
  if (loadRecord) {
    nlapiLogExecution('DEBUG', 'locationLength1');
    locationLength = loadRecord.getLineItemCount('locations');
    nlapiLogExecution('DEBUG', 'locationLength2');
    if (Number(locationLength) > 0) {
      nlapiLogExecution('DEBUG', 'locationLength3');
      for (var j = 1; j < Number(locationLength)+1; j++) {
        nlapiLogExecution('DEBUG', 'locationLength4');
        var locationName = loadRecord.getLineItemValue('locations','location_display',j);
        nlapiLogExecution('DEBUG', 'locationLength5');
        //3PL-PA-XPO East Warehouse
        if (locationName === '3PL-PA-XPO') {
          var eastWarehouseQty = loadRecord.getLineItemValue('locations','quantityavailable',j);
          if (eastWarehouseQty && Number(eastWarehouseQty) > 0) {
            productLocations.eastQty = Number(eastWarehouseQty);
            
          } else {
            productLocations.eastQty = 0;
          }
        }
        
        if (locationName === '3PL-WA-Holm') {
          var westWarehouseQty = loadRecord.getLineItemValue('locations','quantityavailable',j);
          if (westWarehouseQty && Number(westWarehouseQty) > 0) {
            productLocations.westQty = Number(eastWarehouseQty);
          } else {
            productLocations.westQty = 0;
          }
        }
      }
      nlapiLogExecution('DEBUG', 'locationLength4');
      if (productLocations.westQty && productLocations.eastQty) {
        loadRecord.setFieldValues('custitem_warehouse', ["1","2"]);  
        nlapiSubmitRecord(loadRecord);
      } else if (productLocations.eastQty) {
        nlapiSubmitField('lotnumberedinventoryitem',id,'custitem_warehouse', '1');
      } else if (productLocations.westQty) {
        nlapiSubmitField('lotnumberedinventoryitem',id,'custitem_warehouse', '2');
      }
      
    }
    
    
  
  }
  // return productLocations? productLocations : [];
}
